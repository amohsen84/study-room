Stats Service

Overview
=======================
Our learning platform needs to track a user's stats for a particular course. We do this using a stats service.
The task is to create a simple mock of this service. Your stats service needs to provide the capability to create new stats as well as updating stats. The stats managed by the service are created and updated via HTTP calls. Stats are posted on the completion of a learning session and reflect how the user did on the learning session.

Technologies Used
-----------------
    - The service has been written in typescript.
    - Elasticsearch has been used to store/fetch data.


Code Limitation
-----------------
Since it is a simple mock of the service, some options have been dropped such as user authentication, sessionId and courseId validation.


Installation
---------------
 run the following command to install dependencies:

    > cd {Project-Directory}
    > npm install
    > npm install serverless -g


Transpilation
---------------------
The code is written in type script. Run the following commands to transpile it to node JS:

    > cd {Project Directory}
    > npm run build


Project Structure
-----------------
    /src
      /interfaces    # Interfaces definitions.
      /lib           # Classes that facilitate the implementation.
      /middleware    # Middleware
      /route         # Routes definitions (end points)
      /schemas       # Schemas used to validate requests
      /tests         # Test cases definitions
      /utils         # Some help classes.
    /config          # Define configurations such as constants.



API Documentation
---------------------
The API documentation is dynamically generated using swagger. You need to update the swagger.json file when you modify/add an api method. The API documentation is hosted in AWS at:

    https://ml0fkmpnoj.execute-api.eu-west-1.amazonaws.com/dev/api-docs/

It is an interactive documentation that you can use it to test the API.


Deployment to AWS
---------------------
We use serverless to deploy the application to AWS.
To configure serverless with your AWS account, please follow the instructions at:               

    https://serverless.com/framework/docs/providers/aws/guide/credentials/

To deploy Elasticsearch and lambda function, 
    - You need first to enable your IP address to access elasticsearch so you can access elasticsearch locally. You need to update the section related to IP address permissions in elasticsearch-deployment/serverless.yml with your network IP address (This step will be automated in the future).

    - run the following commands:
        To deploy elasticsearch only
            > npm run es-deploy
        To deploy lambda and elasticsearch together:
            > npm run deploy

Deploying Elasticsearch involves creating the index and updating the config file with Elasticsearch endpoint.

The API and elasticsearch are  currently hosted in AWS as follow:
    API Base URL:
        https://ml0fkmpnoj.execute-api.eu-west-1.amazonaws.com/dev/studyroom

    Elasticsearch Endpoint:
        https://search-test-awgy5u3mwxhsrdcdihrbbetscy.eu-west-1.es.amazonaws.com


Running the service locally
-----------------------------------
Before running the service locally, you need first to deploy elasticsearch to AWS.


Testing
---------------------
To run the unit test and generate the coverage document (the document will be generated at {Project Directory}/document), you need to run the following commands:

    > cd {Project Directory}
    > npm test

The coverage document will be generated at {Project Directory}/coverage. It includes details about how the unit tests covered the code showing lines covered and not covered by the unit tests. We are hosting the coverage document at :

    http://bac-docs.s3-website-eu-west-1.amazonaws.com/test_coverage/


Code Documentation
---------------------
We are using TYPE DOC to generate code documentation, The comments need to follow the guidelines specified at               

    http://typedoc.org/guides/doccomments/.

You can generate/update the  code documentation by running the following command:

    > cd {Project Directory}
    > npm run build-doc

You can find the code documentation at the following URL:

    http://bac-docs.s3-website-eu-west-1.amazonaws.com/code_documentation/

