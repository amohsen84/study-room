import { validateSync } from "class-validator";
import { Router } from "express";
import { ElasticSearchWrapper } from "../lib/elasticsearch.wrapper";
import { Logger } from "../lib/logger";
import { validateSessionEvent } from "../middleware/validate.session.event";
import { UpdateCourseStats } from "../schemas/update.course.Stats";

const logger = Logger.getInstance();
const elasticSearchWrapper = ElasticSearchWrapper.getInstance();
const router = Router() as Router;

/** Upsert session event */
router.post("/courses/:courseId", validateSessionEvent, async (req, res) => {
    try {
        const updateRequest = new UpdateCourseStats(req.body || {}, req.header("X-User-Id"), req.params.courseId);
        await elasticSearchWrapper.upsertCourseStats(updateRequest);
        res.status(201).json({response: "The session event has been successfully added/updated."});
    } catch (error) {
        logger.debug(error);
        res.status(500).json({error: "Failed to find the course stats."});
    }
});

/** Get course stats */
router.get("/courses/:courseId", async (req, res) => {
    try {
        const response = await elasticSearchWrapper.getCourseStats(req.header("X-User-Id"), req.params.courseId);
        res.json(response);
    } catch (error) {
        logger.debug(error);
        res.status(500).json({error: "Failed to find the course stats."});
    }
});

/** Get session stats */
router.get("/courses/:courseId/sessions/:sessionId", async (req, res) => {
    try {
        logger.debug("session");
        const response = await elasticSearchWrapper.getSessionStats(req.header("X-User-Id"), req.params.sessionId);
        res.json(response);
    } catch (error) {
        logger.debug(error);
        res.status(500).json({error: "Failed to find the session stats."});
    }
});

export default router;
