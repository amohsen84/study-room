
import * as express from "express";
import home from "./home.router";

export let route = (app: express.Application) => {
    app.use("/studyroom", home);
};
