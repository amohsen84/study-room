
/**
 * The class will be used with ElasticSearchWrapper to return complex queries.
 * 
 * @class  ElasticSearchHelper
 */
export class ElasticSearchHelper {
    /**
     * This function returns the query required to get course stats.
     * @param userID Identifier of the user taking the course.
     * @param courseID Course Identifier.
     * @return query
     */
    public static courseAggQuery(userId: string, courseId: string): any {
       return {
            aggs: {
                avgScore: {
                    avg: {
                        field: "averageScore"  
                    }
                },
                totalModules: {
                    sum: {
                        field: "totalModulesStudied"  
                    }
                },
                totalTime: {
                    sum: {
                        field: "timeStudied"  
                    }
                }
            },
            query: {
                bool: {
                    filter: {
                        bool: {
                            must: [
                                {match: {userId}},
                                {match: {courseId}}
                            ]
                        }
                    }
                }
            },
            size: 0,
        };
    }
    
    /**
     * This function returns the query required to get course stats.
     * @param index index name
     * @return index definition
     */
    public static indexDefinition(indexName: string): any {
        return {
            body: {
                index: indexName,
                mappings: {
                    "session-events": {
                        properties: {
                            averageScore: {
                                type: "float"
                            },
                            courseId: {
                                analyzer: "whitespace",
                                type: "text"
                            },
                            sessionId: {
                                analyzer: "whitespace",
                                type: "text"
                            },
                            timeStudied: {
                                type: "long"
                            },
                            totalModulesStudied: {
                                type: "long"
                            },
                            userId: {
                                analyzer: "whitespace",
                                type: "text"
                            }
                        }
                    }
                },
                settings: {
                    index: {
                        number_of_replicas: 1,
                        number_of_shards: 2
                    }
                }
            },
            index: indexName
        };

    }
}
