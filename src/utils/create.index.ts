
import * as ElasticSearch from "elasticsearch";
import { Logger } from "../lib/logger";
import { ElasticSearchHelper } from "../utils/elasticsearch.helper";

const logger = Logger.getInstance();

export function createIndex(config) {
    const definition = ElasticSearchHelper.indexDefinition(config.index);
    const client = new ElasticSearch.Client({  
        hosts: [
            `https://${config.host}`
        ]
    });
    client.indices.create(definition, (error, res) => {
        if (error) {
            logger.debug(error.message);
        }
    });
}
