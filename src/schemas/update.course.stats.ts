import {IsInt, IsNumber, IsUUID, Min } from "class-validator";
import { Logger } from "../lib/logger";

const logger = Logger.getInstance();

/**
 * The class represent event details of a session completed by a learner.
 * Class validators are used for each field so the request can be validated against the specified criteria.
 * 
 * @class  UpdateCourseStats
 */
export class UpdateCourseStats {

    /** User ID */
    @IsUUID()
    public userId: string;

    /** Course ID */
    @IsUUID()
    public courseId: string;

    /** Session ID */
    @IsUUID()
    public sessionId: string;

    /** Number of modules studied */
    @IsInt()
    @Min(1)
    public totalModulesStudied: any;

    /** Average score achieved in the session */ 
    @IsNumber()
    @Min(0)
    public averageScore: any;
    
    /** Time spent in the session (in milliseconds) */ 
    @IsInt()
    @Min(0)
    public timeStudied: any;

    public constructor(body: any, userId: string, courseId: string) {
        this.userId = userId;
        this.courseId = courseId;
        this.sessionId = body.sessionId;
        this.totalModulesStudied = body.totalModulesStudied;
        this.averageScore = body.averageScore;
        this.timeStudied = body.timeStudied; 
    }
}
