/**
 * This interface represents elasticsearch config. 
 * @interface  IElasticSearchConfig
 */
export interface IElasticSearchConfig  {
    /** AWS Region */
    region: string;
    /** index name */
    index: string;
    /** Service Endpoint */
    host: string;
}
