/**
 * This interface represents stats data. 
 * @interface  IStats
 */
export interface IStats {

    /** Number of modules studied */
    totalModulesStudied: number;

    /** Average score achieved in the session */ 
    averageScore: number;
    
    /** Time spent in the session (in milliseconds) */ 
    timeStudied: number;
}
