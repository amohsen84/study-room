import { IStats } from "./IStats";
/**
 * This interface represents session stats data. 
 * @interface  ISessionStats
 */
export interface ISessionStats extends IStats {
    /** Session ID */
    sessionId: string;
}
