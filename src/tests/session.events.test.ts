import * as config from "config";
import { cloneDeep } from "lodash";
import "mocha";
import { testApplication as app } from "../testrunner";
import { successfulData, unsuccessfulDataFields } from "./session.events.data";

describe("Upserting session events requests", () => {

    function testPost(request: string, expectedHttpResponseCode: number = 200, postData: any, contentType: RegExp = /json/) {
        return app.post(request)
            .set("Content-Type", "application/json")
            .set("X-User-Id", "be92d6e8-8ddb-11e8-9eb6-529269fb1459")
            .send(postData)
            .expect(expectedHttpResponseCode)
            .expect("content-type", contentType);
    }

    describe("POST successful data", () => {
        it("POST Should return a successful response", done => {
            testPost("/studyroom/courses/be92d6e8-8ddb-11e8-9eb6-529269fb9999", 201, successfulData)
                .end((err, res) => err ? done(err) : done());
        });
    });

    describe("POST unsuccessful data", () => {
        // Iterate over the count of the fields on successfulData
        for (const field in unsuccessfulDataFields) {
            if (unsuccessfulDataFields.hasOwnProperty(field)) {
                // Run a test for each of these fields
                describe("POST unsuccessful field: " + field, () => {
                    const data = unsuccessfulDataFields[field];
                    // Iterate over each type of incorrect data property
                    for (const prop in data) {
                        if (data.hasOwnProperty(prop)) {
                            // Clone the successful data object with incorrect field value
                            const dataClone = cloneDeep(successfulData);
                            const overrideObject = data[prop];
                            for (const key in overrideObject) {
                                if (overrideObject.hasOwnProperty(key)) {
                                    dataClone[key] = overrideObject[key];
                                }
                            }
                            // Perform test
                            it("Should return an unsuccessful response from " + field + " " + prop, done => {
                                testPost("/studyroom/courses/be92d6e8-8ddb-11e8-9eb6-529269fb9999", 400, dataClone)
                                    .end((err, res) => err ? done(err) : done());
                            });
                        }
                    }
                });
            }
        }
    });
});

describe("Get Stats", () => {

    function testGet(request: string, expectedHttpResponseCode: number = 200, postData: any, contentType: RegExp = /json/) {
        return app.get(request)
            .set("Content-Type", "application/json")
            .set("X-User-Id", "be92d6e8-8ddb-11e8-9eb6-529269fb1459")
            .expect(expectedHttpResponseCode)
            .expect("content-type", contentType);
    }

    describe("Get Course Stats", () => {
        it("Get Should return a successful response", done => {
            testGet("/studyroom/courses/be92d6e8-8ddb-11e8-9eb6-529269fb1458/", 200, successfulData)
                .end((err, res) => err ? done(err) : done());
        });
    });

    describe("Get Session Stats", () => {
        it("Get Should return a successful response", done => {
            testGet("/studyroom/courses/be92d6e8-8ddb-11e8-9eb6-529269fb1458/sessions/be92d6e8-8ddb-11e8-9eb6-529269fb1460", 200, successfulData)
                .end((err, res) => err ? done(err) : done());
        });
    });
});
