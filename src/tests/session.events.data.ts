export const successfulData = {
    averageScore: 27, 
    sessionId: "be92d6e8-8ddb-11e8-9eb6-529269fb9876",
    timeStudied: 4000,
    totalModulesStudied: 4
};

// individual field replacement
export const unsuccessfulDataFields = {
    averageScore: {
        "Negative figure": {
            averageScore: -88
        },
        "Wrong type": {
            averageScore: "wrong type",
        },
        "missing": {
            averageScore: undefined,
        }
    },
    sessionId: {
        "Wrong type": {
            sessionId: "wrong type",
        },
        "missing": {
            sessionId: undefined,
        }
    },
    timeStudied: {
        "Negative figure": {
            timeStudied: -88
        },
        "Wrong type": {
            timeStudied: "wrong type",
        },
        "missing": {
            timeStudied: undefined,
        }
    },
    totalModulesStudied: {
        "Negative figure": {
            totalModulesStudied: -88
        },
        "Wrong type": {
            totalModulesStudied: "wrong type",
        },
        "missing": {
            totalModulesStudied: undefined,
        }
    },
};
