
import * as jsonfile from "jsonfile";
import { Logger } from "./lib/logger";
import { createIndex } from "./utils/create.index";

const logger = Logger.getInstance();

/**
 * This method is used to initialize Elasticsearch config
 */
export function handler(data, serverless, options) {
    const file = "../config/default.json";
    const obj = {  elasticSearch: {
                        host: data.ServiceEndpoint, 
                        index: data.Index,
                        region: data.Region,
                    }
                };
    // Updating the config file    
    jsonfile.writeFile(file, obj,  (err) => {
        if (err) {
            logger.debug("An error occurred while updating the config file.", err);
        } else {
            // Creating an index with appropriate settings and mappings
            createIndex(obj.elasticSearch);
        }
    });
}
