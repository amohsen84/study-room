import { NextFunction, Request, Response, Router } from "express";
import { Logger } from "../lib/logger";

const logger = Logger.getInstance();

/**
 * Logger middleware
 * Logs the request method, path and response code
 *
 * @param req Express Request
 * @param res Express Response
 * @param next Express Next Function
 */
export function logHTTP(req: Request, res: Response, next: NextFunction) {
    function requestLogger() {
        res.removeListener("finish", requestLogger);
        res.removeListener("close", requestLogger);
        logger.info(`${res.statusCode} ${req.method} ${req.url}`);
    }
    res.on("finish", requestLogger);
    res.on("close", requestLogger);
    next();
}
