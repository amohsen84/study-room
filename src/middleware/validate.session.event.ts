import { validateSync } from "class-validator";
import { NextFunction, Request, Response, Router } from "express";
import { Logger } from "../lib/logger";
import { UpdateCourseStats } from "../schemas/update.course.Stats";

const logger = Logger.getInstance();

/**
 * validateSessionEvent middleware
 * Validate whether the request sent is valid.
 *
 * @param req Express Request
 * @param res Express Response
 * @param next Express Next Function
 */
export function validateSessionEvent(req: Request, res: Response, next: NextFunction) {
    try {
        const courseId = req.params.courseId;
        const updateRequest = new UpdateCourseStats(req.body || {}, req.header("X-User-Id"), req.params.courseId);
        const errors = validateSync(updateRequest);
        if (errors.length) {
            logger.debug(errors);
            res.status(400).json({ error: "Invalid request."});
        } else {
            next();
        }
    } catch (error) {
        logger.debug(error);
        res.status(400).json({ message: "Invalid request."});
    }
}
