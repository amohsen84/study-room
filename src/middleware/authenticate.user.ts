import { Validator } from "class-validator";
import { NextFunction, Request, Response, Router } from "express";
import { Logger } from "../lib/logger";
import { UpdateCourseStats } from "../schemas/update.course.Stats";

const logger = Logger.getInstance();

/**
 * Authenticate User
 * This is a dummy function as this is a mock-up code (for demo purposes)
 * In reality, this function should  check J-Token passed with the request.
 * 
 *
 * @param req Express Request
 * @param res Express Response
 * @param next Express Next Function
 */
export function authenticateUser(req: Request, res: Response, next: NextFunction) {
    const validator = new Validator();
    if (validator.isUUID(req.header("X-User-Id"))) {
        next();
    } else {
        res.status(403).json({error: "Unauthorized Access."});
    }
}
