// This is the root file for the whole application
// The application is imported and served using submitted arguments
import { argv } from "yargs";
import { Application } from "./lib/application";
import { Logger } from "./lib/logger";

// CLI Arguments
const PORT = argv.port || 1337;
const HOST = argv.host || "127.0.0.1";

// Get instances
const logger = Logger.getInstance();
const app = Application.getInstance();

// Serve application
const httpServer = app.listen(PORT, HOST, () => logger.info(`Serving application on http://${HOST}:${PORT}`));

const terminateConnection = () => {

    try {

        if (httpServer) {
            httpServer.close();
        }

    } catch (err) {
    }
};

process.on("exit", terminateConnection);
process.on("unhandledRejection", terminateConnection);
process.on("uncaughtException", terminateConnection); // unexpected crash
process.on("SIGINT", terminateConnection); // Ctrl + C (1)
