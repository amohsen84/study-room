import * as Aws from "aws-sdk";
import * as config from "config";
import * as crypto from "crypto";
import * as ElasticSearch from "elasticsearch";
import * as HttpAwsEs from "http-aws-es";
import { IElasticSearchConfig } from "../interfaces/IElasticSearchConfig";
import { ISessionStats } from "../interfaces/ISessionStats";
import { IStats } from "../interfaces/IStats";
import { UpdateCourseStats } from "../schemas/update.course.stats"; 
import { ElasticSearchHelper } from "../utils/elasticsearch.helper"; 
import { Logger } from "./logger";

const logger = Logger.getInstance();
// Get AWS elastic search config
const elasticSearchConfig = config.get<IElasticSearchConfig>("elasticSearch") as IElasticSearchConfig;
// Update region
Aws.config.update({
    region: elasticSearchConfig.region
});

/**
 *  This class is responsible to communicate with elasticsearch to index and query data
 * 
 * @class  ElasticSearchWrapper
 */
export class ElasticSearchWrapper {

    /**
     * Get/create a singleton ElasticSearchWrapper instance
     */
    public static getInstance(): ElasticSearchWrapper {
        if (!ElasticSearchWrapper.instance) {
            ElasticSearchWrapper.instance = new ElasticSearchWrapper();
        }
        return ElasticSearchWrapper.instance;
    }

    /** ElasticSearchClient singleton instance */
    private static instance: ElasticSearchWrapper;

    /** ElasticSearchClient */
    private client: ElasticSearch.Client;

    /**
     * Constructor to initialize an ElasticSearchWrapper object
     */
    private constructor() {
        // Reading elastic search from the environment
        const esEndpoint = `https://${elasticSearchConfig.host}`;
        // create an elasticsearc client object
        this.client = new ElasticSearch.Client({  
            connectionClass: HttpAwsEs,
            hosts: [
                esEndpoint
            ]
        });
    }

    /**
     * Upserting a session event document
     * @param stats session event details
     */  
    public async upsertCourseStats(stats: UpdateCourseStats): Promise<void>  {      
        const response = await this.client.index({  
            body: stats,
            id: crypto.createHash("md5").update(`${stats.sessionId}${stats.userId}`).digest("hex"),
            index: elasticSearchConfig.index,
            type: "session-events",
          });
        logger.debug(response);
    }

    /**
     * Upserting a session event document
     * @param userID Identifier of the user taking the course.
     * @param courseID Course Identifier.
     * @return course stats
     */  
    public async getCourseStats(userId: string, courseId: string): Promise<IStats> {
        const response = await this.client.search({
            body: ElasticSearchHelper.courseAggQuery(userId, courseId),
            index: elasticSearchConfig.index,
            type: "session-events"
          });
        return {
            averageScore: response.aggregations.avgScore.value,
            timeStudied: response.aggregations.totalTime.value,
            totalModulesStudied: response.aggregations.totalModules.value,
        } as IStats;
    }

    /**
     * Upserting a session event document
     * @param userID user Identifier.
     * @param sessionID session Identifier.
     * @return session stats
     */  
    public async getSessionStats(userId: string, sessionId: string): Promise<ISessionStats> {
        const response = await this.client.get({
            id: crypto.createHash("md5").update(`${sessionId}${userId}`).digest("hex"),
            index: elasticSearchConfig.index,
            type: "session-events",
        });
        return response._source as ISessionStats;
    }
}
