import * as bodyParser from "body-parser";
import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import { HttpError, InternalServerError, NotFound } from "http-errors";
import * as  path from "path";
import * as swaggerUi from "swagger-ui-express";
import * as document from "../../swagger.json";
import { authenticateUser } from "../middleware/authenticate.user";
import { logHTTP } from "../middleware/logger.middleware";
import * as router from "../route";
import { Logger } from "./logger";

const logger = Logger.getInstance();

/**
 *  The main API application instance
 *  Here middleware are assigned to parse POST data and log request/response info
 *  Also handlers are assigned for logging and returning errors
 * @class  Application
 */
export class Application {
    
    /**
     * Get/create a singleton HTTP Application instance
     */
    public static getInstance(): express.Application {
        if (this.instance) {
            return this.instance;
        }
        // Create application instance
        this.instance = express();
        // Parse JSON or URL encoded body data
        this.instance.use(bodyParser.json());
        this.instance.use(bodyParser.urlencoded({ extended: true }));
        // Use Swagger
        const showExplorer = false;
        const options = {};
        const customCss =  ``;
        this.instance.use("/api-docs", swaggerUi.serve, swaggerUi.setup(document, showExplorer, options, customCss, "/favicon.png", null, "API document"));
        // Log request/response middleware
        this.instance.use(logHTTP);
        // Authenticate User
        this.instance.use(authenticateUser);
        // Load from router modules
        router.route(this.instance);
        // 404 Not Found
        // If no previous route handler has matched the request, this one is called
        this.instance.use((req, res) => {
            if (!res.headersSent) {
                throw new NotFound();
            }
        });
        // Error handler
        // This catches any error thrown in the aplication
        // If the error is an HttpError, it is used in the response
        // For all other errors, the error is logged and an Internal Server Error is returned
        this.instance.use((err: HttpError | Error, req: Request, res: Response, next: NextFunction) => {
            
            if (err instanceof HttpError) {
                // Respond with thrown HTTP Errors
                res.status(err.statusCode);
                res.jsonp({ error: { message: err.message } });
            } else {
                // Log other Errors and respond with Internal Server Error
                logger.error(err);
                const ise = new InternalServerError();
                res.status(ise.statusCode);
                res.jsonp({ message: ise.message });
            }
        });
        return this.instance;
    }
    
    /** Express application singleton instance */
    private static instance: express.Application;
}
